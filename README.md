# yt2rss

## Name
The name of the project is a short for "YouTube to RSS".

## Description
It's just a bash script that uses cURL and xmllint to find a channel's ID and concatenate to the universal RSS feed url from YouTube (https://www.youtube.com/feeds/videos.xml?channel_id=CHANNEL_ID).

## Installation
You don't need to install anything, it's just a regular bash script. But, if you want to, you can make a symbolic in ~/.local/bin using the following command:
```
ln -s $(readlink -f yt2rss) ~/.local/bin/yt2rss
```

## Usage
Copy the channel's URL and paste it as an argument.

Example usage:
```
./yt2rss URL
```

Or, if you made the symbolic link:
```
yt2rss URL
```

## Contributing
This is just a regular bash script, no need for contributions. But, if you have a good idea and wants to improve it, feel free to contribute.
Obs: It's not guaranteed that your improvements will be accepted and merged. It's a personal script, I won't treat it as a project.

Feel free to fork it, if you want to.

## Authors and acknowledgment
It was made by me (Darwin Brandão), and you can use it freely, without any kind of attribution. 

Intellectual Property is an artificial abomination. You can use it without any attributions, for personal, comercial and any other kind of use you can think of.

Just copy and paste into your projects and don't bother me with questions regarding to attribution, credit and anything like that.

## License
MIT License.

## Project status
This works very well for my needs, but I have some improvements to do.
